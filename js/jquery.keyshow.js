// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ( $, window, document, undefined ) {


	// undefined is used here as the undefined global variable in ECMAScript 3 is
	// mutable (ie. it can be changed by someone else). undefined isn't really being
	// passed in so we can ensure the value of it is truly undefined. In ES5, undefined
	// can no longer be modified.

	// window and document are passed through as local variable rather than global
	// as this (slightly) quickens the resolution process and can be more efficiently
	// minified (especially when both are regularly referenced in your plugin).

	// Create the defaults once
	var pluginName = "keyShow";
	var defaults = {
		modifierSeparator: "+",
        includeSeparator: true,
        specialKeys: [
            "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "F13", "F14", "F15",
            "Esc",
            "Option",
            "Alt",
            "Delete",
            "Backspace",
            "Command",
            "Control",
            "Ctrl",
            "Space Bar",
            "Shift",
            "Up Arrow",
            "Right Arrow",
            "Down Arrow",
            "Left Arrow",
            "Page Up",
            "Page Down",
            "Home",
            "End",
            "Tab",
            "Enter",
            "Return",
            "Windows"
        ],
        replacementTable: {
            "command": "&#8984;",
            "shift": "&#8679;",
            "up arrow": "&uarr;",
            "right arrow": "&rarr;",
            "down arrow": "&darr;",
            "left arrow": "&larr;"
        }
	};


	// The actual plugin constructor
	function KeyShow ( element, options ) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
	}

	// Avoid Plugin.prototype conflicts
	$.extend(KeyShow.prototype, {
        init: function () {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.settings
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.settings).
            self = this;
            $(this.element).each(function() {
                $(this).html(self.replaceContent($(this).html()));
                $(this).find(':contains(Space Bar)').each(function() {
                    $(this).addClass('key-space-bar');
                });
            });
        },
        replaceContent: function(content) {
            self = this;

            re = new RegExp('(-?)(' + self.settings.specialKeys.join('|') + '|(?:[^\\w]([a-zA-Z0-9\\[\\]\+\\-,])(?![\\w])))', 'g');
            content = content.replace(re, function(match, $1, $2, $3) {
                separatorString = (self.settings.includeSeparator) ? self.settings.modifierSeparator : "";
                matchedKey = ($2[0] == self.settings.modifierSeparator) ? $3[0] : $2;
                matchedSeparator = ($2[0] == self.settings.modifierSeparator || $1 == self.settings.modifierSeparator) ? separatorString : "";

                for (key in self.settings.replacementTable) {
                    if (matchedKey.toLowerCase() == key) {
                        matchedKey = self.settings.replacementTable[key];
                    }
                }
                return matchedSeparator + "<span class=\"key\">" + matchedKey + "</span>";
            });
            return content;
        }
	});

	$.fn[pluginName] = function (options) {
        return this.each(function() {
            if ( !$.data( this, "plugin_" + pluginName)) {
                    $.data( this, "plugin_" + pluginName, new KeyShow(this, options));
            }
        });
	};

})( jQuery, window, document );
