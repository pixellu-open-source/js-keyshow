# KeyShow -- Keyboard Shortcuts Beautifier
   
## Usage Example

HTML code:
    
    ...
    
    <link rel="stylesheet" type="text/css" href="css/jquery.keyshow.css">
    <script "js/jquery.keyshow.js"></script>

    ...
    
    <tr>
        <td> Preview image using Quick Look </td>
        <td> Space Bar </td>
    </tr>
    <tr>
        <td> Import images </td>
        <td> Command-I </td>
    </tr>
    <tr>
        <td> Delete selected item(s) </td>
        <td> Delete </td>
    </tr>
    <tr>
        <td> Add image(s) to current spread </td>
        <td> Command-Up Arrow </td>
    </tr>
    <tr>
        <td> Show all images </td>
        <td> Command-Control-A </td>
    </tr>
    ...

Now run the JS piece:

    <script>
        $(document).ready(function() {
            $('article table td:odd').keyShow({modifierSeparator: "-", includeSeparator: true});
        })
    </script>
    
All your keyboard shortcuts are looking so much sexier now!